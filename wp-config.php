<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'loft');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'mIwwz[+8H$|y%!6Pv0B+L//AF@fj9}TK?7ByRlG-pqRh;7nj,or.D[D{i-N;hV;`');
define('SECURE_AUTH_KEY',  '/&w&%%XK?LR/qJ?``;Oe+:`H/gv2Cd;_.6e_9g+CBI_pY+jDPr|xW(*iB#]V5/Qd');
define('LOGGED_IN_KEY',    'Lt6}e-L]pB6TP`EMG3ARy;z<}G&]uuXR)@b%GCKi.t^d!hs bRb+;EX! 8GmU<n[');
define('NONCE_KEY',        'y{EC$8p p>qKukZK0cgZD iM%NO%@xLGCg=VV$%Bo87,N2!L.A#3o.g|_j.:n,t;');
define('AUTH_SALT',        '0Dr#*HR[h.fNjT+M@x</dcBbFH5Z1?AniL.2NXF~k}dp`-z6.0 fC%7[JVjT?JR}');
define('SECURE_AUTH_SALT', 'Y{}q6VhaJ(:H>lm?joxWAtqN.Y;I.6mM)>x&5mNKe1_o6]I[&KlcBt{ ~4zJvty5');
define('LOGGED_IN_SALT',   '8QilM{wG[HKOx6FEmz3f^2h@%iIHs_z`Ea 3&&P_Plc 2`(jGc-v$58FJ`<Bo(<J');
define('NONCE_SALT',       'x8~so|QCunuEjVbFp%pRn3p Nkr$noAq.Y2QF7#}|xh3st5(H(<5/qIArYQn*czQ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
