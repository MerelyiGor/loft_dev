<?php
/**
 * The template for displaying the header
 */

?>
<!DOCTYPE html>
<html <?php echo ICL_LANGUAGE_CODE ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php bloginfo('name'); ?> |
        <?php is_home() ? bloginfo('description') : wp_title(''); ?>
    </title>
    <meta name="viewport"
          content="width=device-width,  initial-scale=1, maximum-scale=1.0, user-scalable=0, minimal-ui"/>

    <?php wp_head(); ?>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body <?= body_class(); ?>>
<header class="header">
    <?php include(TEMPLATEPATH . '/part/svg/header-logo.php'); ?>
    <div class="headerMenu">
        <?php wp_nav_menu('menu_class=tmenu&theme_location=top'); ?>
    </div>
    <div id="nav-icon1" class="headerMenu2 prevent">
        <p>Меню</p>
        <div id="nav-icon2">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="menu">
        <div class="titl">Навигация</div>
        <?php wp_nav_menu('menu_class=bmenu&theme_location=left'); ?>
        <?php wp_nav_menu('menu_class=r-ul&theme_location=right'); ?>
        <div class="bot-inform">© 2018 The loft, Inc.</div>
        <div class="bg-menu">МЕНЮ</div>
    </div>

</header>
