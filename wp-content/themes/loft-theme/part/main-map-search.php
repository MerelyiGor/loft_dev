<?php
if (is_front_page()) {
    ?>

    <script>
        function initMap() {
            var centerLatLng = new google.maps.LatLng(50.463696, 30.513968);
            // Обязательные опции с которыми будет проинициализированна карта
            var mapOptions = {
                center: centerLatLng, // Координаты центра мы берем из переменной centerLatLng
                zoom: 11,               // Зум по умолчанию. Возможные значения от 0 до 21
                styles:[
                    { "featureType": "all", "elementType": "labels.text.fill",
                        "stylers": [
                            {"saturation": 36},
                            {"color": "#333333"},
                            {"lightness": 40}
                        ]
                    },
                    {"featureType": "all","elementType": "labels.text.stroke",
                        "stylers": [
                            {"visibility": "on"},
                            {"color": "#ffffff"},
                            {"lightness": 16}
                        ]
                    },
                    {"featureType": "all","elementType": "labels.icon",
                        "stylers": [
                            {"visibility": "off"}
                        ]
                    },
                    {"featureType": "administrative","elementType": "geometry.fill",
                        "stylers": [
                            {"color": "#f7f7f7"},
                            {"lightness": 20}
                        ]
                    },
                    {"featureType": "administrative","elementType": "geometry.stroke",
                        "stylers": [
                            {"color": "#f7f7f7"},
                            {"lightness": 17},
                            {"weight": 1.2}
                        ]
                    },
                    {"featureType": "landscape","elementType": "geometry",
                        "stylers": [
                            {"color": "#f7f7f7"},
                            {"lightness": 20}
                        ]
                    },
                    { "featureType": "poi","elementType": "geometry",
                        "stylers": [
                            {"color": "#f5f5f5"},
                            {"lightness": 21}
                        ]
                    },
                    {"featureType": "poi.park","elementType": "geometry",
                        "stylers": [
                            {"color": "#dedede"},
                            {"lightness": 21}
                        ]
                    },
                    {"featureType": "road.highway","elementType": "geometry.fill",
                        "stylers": [
                            {"color": "#ffffff"},
                            {"lightness": 17}
                        ]
                    },
                    {"featureType": "road.highway","elementType": "geometry.stroke",
                        "stylers": [
                            { "color": "#ffffff"},
                            {"lightness": 29},
                            {"weight": 0.2}
                        ]
                    },
                    {"featureType": "road.arterial","elementType": "geometry",
                        "stylers": [
                            {"color": "#ffffff"},
                            {"lightness": 18}
                        ]
                    },
                    {"featureType": "road.local","elementType": "geometry",
                        "stylers": [
                            {"color": "#ffffff"},
                            {"lightness": 16}
                        ]
                    },
                    {"featureType": "transit","elementType": "geometry",
                        "stylers": [
                            {"color": "#f2f2f2"},
                            {"lightness": 19}
                        ]
                    },
                    {"featureType": "water","elementType": "geometry",
                        "stylers": [
                            {"color": "#99ccc0"},
                            {"lightness": 17}
                        ]
                    }
                ]
            };
            window.map = new google.maps.Map(document.getElementById("map"), mapOptions);
        }

        if(window.location.href.split('?')[1] !== undefined){
            var parsed = new Object;
            window.location.href.split('?')[1].split('&').forEach(function(element){ parsed[element.split('=')[0]] = element.split('=')[1]});
            window.product_id = parsed.buy_product_id;
        }
        
        window.mapMarkerImage = "<?= get_template_directory_uri() . '/media/img/mark1.png' ?>";
        window.mapMarkerImageActive = "<?= get_template_directory_uri() . '/media/img/mark.png' ?>";
    </script>
    <script async defer  src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyAb0QGeFu5sD7h3P_MeHoGrmUZqT97UOSk&callback=initMap"type="text/javascript"></script>
    <?
}
?>