<?php
/*
Template Name: Корпоративным клиентам
*/
get_header();?>

    <main class="open-cooperation open-cooperation_cl">
        <div class="breadcrumbs-wrapper">
            <ul class="breadcrumbs">
                <?= the_theme_loft_breadcrumb() ?>
            </ul>
        </div>
        <div class="section1">
            <div class="section1-left">
                <div class="section1-img" style="background-image: url(<?= get_field('sfhhhdf') ?>)"></div>
            </div>
            <div class="section1-right">
                <span class="section1-title">Сотрудничество</span>
                <h2 class="section1-heading"><?= get_field('title-sdh') ?></h2>
                <p class="section1-text"><?= get_field('txt-sdh') ?></p>
                <p class="section1-note"><?= get_field('txt-sdh2') ?></p>
                <div class="section1-btn-wrapper">
                    <a href="<?= get_field('url-sdfgf') ?>" class="btn">Написать</a>
                </div>
            </div>
            <div class="section1-bg-text">Кор</div>
        </div>

    </main>
<?php
get_footer();