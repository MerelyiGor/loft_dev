<?php
get_header(); ?>
    <main class="error">

        <img class="desc" src="<?= get_template_directory_uri(); ?>/media/img/bg404.png" alt="404">
        <img class="tab" src="<?= get_template_directory_uri(); ?>/media/img/bg404-768.png" alt="404">
        <img class="mob" src="<?= get_template_directory_uri(); ?>/media/img/bg404-320.png" alt="404">
        <div class="text-block">
            <div class="titl">Страница не найдена</div>
            <div class="text">Проверьте правильность адреса
                или нажмите кнопку ниже</div>
            <a >Главная страница</a>
        </div>

    </main>
<!--    <section class="404">-->
<!--		<div class="container404">-->
<!--			<h2 class="title404">Такой страницы не существует</h2>-->
<!--		</div>-->
<!--	</section>-->
<?php
get_footer();