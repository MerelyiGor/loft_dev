<?php
/*
Template Name: Сотрудничество
*/
get_header(); ?>
    <main class="home">

        <div id="section2" class="section2" style="padding-top: 20px!important;">

            <div class="breadcrumbs-wrapper">
                <ul class="breadcrumbs">
                    <?= the_theme_loft_breadcrumb() ?>
                </ul>
            </div>

            <div class="l-block l-command" style="transform: translateX(0px);    margin-left: 0;">
                <img style="width: 820.3px; height: 698px; object-fit: cover;" class="s2-comm-img" src="<?= get_field('img-comnash'); ?>" alt="">
                <img style="width: 820.3px; height: 698px; object-fit: cover;" class="s2-hist-img" src="<?= get_field('img-history'); ?>" alt="">
                <img style="width: 820.3px; height: 698px; object-fit: cover;" class="s2-achiev-img" src="<?= get_field('img-dostij'); ?>" alt="">
            </div>



            <div class="r-block" style="transform: translateX(0px);">
                <div class="titl"><?= get_field('title-block1'); ?></div>
                <ul>
                    <li class="command-b activ-s2-b"><a>Диллерам</a></li>
                    <li class="history-b"><a>Дизайнерам</a></li>
                    <li class="achievements-b"><a>Корпоративным клиентам</a></li>
                </ul>
            </div>


            <div class="bot-block" style="visibility: visible; opacity: 1;">
                <div class="command bot-text activ-s2">
                    <div class="titl">Диллерам</div>
                    <p><?= get_field('tx_hjbv-1'); ?></p>
                    <a href="<?= get_field('url_sevibu'); ?>"> Подробнее
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="15" viewBox="0 0 28 15">
                            <path fill-rule="evenodd" d="M28 7l-8-7-2 2 5 4H0v2h23l-5 5 2 2z"/>
                        </svg>
                    </a>
                </div>
                <div class="history bot-text">
                    <div class="titl">Дизайнерам</div>
                    <p><?= get_field('tx_hjbv-2'); ?></p>
                    <a href="<?= get_field('url_sevibu_2'); ?>"> Подробнее
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="15" viewBox="0 0 28 15">
                            <path fill-rule="evenodd" d="M28 7l-8-7-2 2 5 4H0v2h23l-5 5 2 2z"/>
                        </svg>
                    </a>
                </div>
                <div class="achievements bot-text">
                    <div class="titl">Корпоративным клиентам</div>
                    <p><?= get_field('tx_hjbv-3'); ?></p>
                    <a href="<?= get_field('url_sevibu_3'); ?>"> Подробнее
                        <svg xmlns="http://www.w3.org/2000/svg" width="28" height="15" viewBox="0 0 28 15">
                            <path fill-rule="evenodd" d="M28 7l-8-7-2 2 5 4H0v2h23l-5 5 2 2z"/>
                        </svg>
                    </a>
                </div>

            </div>

            <div class="bg-paralax" id="bg-parallax-1">СОТРУДН</div>
        </div>

    </main>


<?php
get_footer();
