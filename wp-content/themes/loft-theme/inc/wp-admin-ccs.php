<?php

/**
 * Изминения логотипа при регистрации или входе в админ-панель wordpress
 * ---------------------------------------------------------------------------------------------------------------------
 */
function my_custom_css_login_registrations(){
    echo '
   <style type="text/css">
   
    .login h1 a {
     background: url('. get_bloginfo('template_directory') .'/media/img/custom/logo.jpg) no-repeat 0 0 !important;
     background-size: cover!important;
     } 
     
     
    </style>';
}
add_action('login_head', 'my_custom_css_login_registrations');



/* Ставим ссылку с логотипа на сайт, а не на wordpress.org */
add_filter( 'login_headerurl', create_function('', 'return get_home_url();') );
