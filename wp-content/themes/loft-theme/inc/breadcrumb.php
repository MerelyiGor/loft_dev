<?php
/**
 * Хлебные крошки
 * ---------------------------------------------------------------------------------------------------------------------
 */
function the_theme_loft_breadcrumb()
{

	global $post;
	if (!is_home()) {
		echo '';
		if (is_single()) { // записи
			the_category(', ');

            echo '<li class="breadcrumbs-item"><a href="/">Главная</a></li>';
            echo '<li class="breadcrumbs-item"><a href="/'.get_page_uri('101').'">Категории</a></li>';
            echo '<li class="breadcrumbs-item breadcrumbs-shortcut"><a href="'.get_category_link( wp_get_post_terms(get_the_ID(), ['taxonomy'=>'card_category'])[0]->term_id ).'">';
            echo wp_get_post_terms(get_the_ID(), ['taxonomy'=>'card_category'])[0]->name;
            echo '</a></li>';
            echo '<li class="breadcrumbs-item breadcrumbs-shortcut"><a style="cursor: default">';
            echo the_title();
            echo '</a></li>';

		}
        elseif ( is_page('185') || is_page('169') || is_page('161')) {
            echo '<li class="breadcrumbs-item"><a href="/">Главная</a></li>';
            echo '<li class="breadcrumbs-item breadcrumbs-shortcut"><a href="'.get_page_uri( '241' ).'">';
            echo get_the_title( 241 );
            echo '</a></li>';echo '<li class="breadcrumbs-item breadcrumbs-shortcut"><a style="cursor: default">';
            echo the_title();
            echo '</a></li>';
        }
		elseif (is_page()&!is_front_page()) {
            echo '<li class="breadcrumbs-item"><a href="/">Главная</a></li>';
            echo '<li class="breadcrumbs-item breadcrumbs-shortcut"><a style="cursor: default">';
            echo the_title();
            echo '</a></li>';
        }
		elseif ($categories = get_terms($args = array(
            'taxonomy' => 'card_category'
        ))) {
            echo '<li class="breadcrumbs-item"><a href="/">Главная</a></li>';
            echo '<li class="breadcrumbs-item"><a href="/'.get_page_uri('101').'">Категории</a></li>';
            echo '<li class="breadcrumbs-item breadcrumbs-shortcut"><a style="cursor: default">';
            echo single_cat_title();
            echo '</a></li>';

		}

//		elseif (is_search()) { // страницы поиска
//			echo esc_html__('Результаты поиска по запросу "', 'owl_blog') . get_search_query() . '"';
//		}
//
//		elseif (is_404()) { // если станица не существует
//			echo '<li>' . esc_html__('Страница не найдена', 'owl_blog') . '</li>';
//		}

	}
}



