<?php
/**
 * Отправка данных AJAX из формы отправки данных
 * ---------------------------------------------------------------------------------------------------------------------
 */

/************** ------- Подключаю myajax от wordpress к скриптам темы -- должно вывести в скриптах скрипт myajax ------- **************/
add_action( 'wp_enqueue_scripts', 'myajax_data', 99 ); //подключаю скрипты myajax к теме через свои скрипты - скрипты подключать только через  wp_enqueue_scripts
function myajax_data(){


    wp_localize_script( 'my-custom', 'myajax',  // подключаю myajax с скрипту с хендллером main.js
        array(
            'url' => admin_url('admin-ajax.php') //забираю данные js которые в js файле передаются в wp http:\/\/blufixx\/wp-admin\/admin-ajax.php
        )
    );

}



class ReCaptchaResponse
{
    public $success;
    public $errorCodes;
}
class ReCaptcha
{
    private static $_signupUrl = "https://www.google.com/recaptcha/admin";
    private static $_siteVerifyUrl = "https://www.google.com/recaptcha/api/siteverify?";
    private $_secret;
    private static $_version = "php_1.0";
    /**
     * Constructor.
     *
     * @param string $secret shared secret between site and ReCAPTCHA server.
     */
    public function __construct($secret)
    {
        if ($secret == null || $secret == "") {
            die("To use reCAPTCHA you must get an API key from <a href='"
                . self::$_signupUrl . "'>" . self::$_signupUrl . "</a>");
        }
        $this->_secret = $secret;
    }
    /**
     * Encodes the given data into a query string format.
     *
     * @param array $data array of string elements to be encoded.
     *
     * @return string - encoded request.
     */
    private function _encodeQS($data)
    {
        $req = "";
        foreach ($data as $key => $value) {
            $req .= $key . '=' . urlencode(stripslashes($value)) . '&';
        }
        // Cut the last '&'
        $req=substr($req, 0, strlen($req)-1);
        return $req;
    }
    /**
     * Submits an HTTP GET to a reCAPTCHA server.
     *
     * @param string $path url path to recaptcha server.
     * @param array  $data array of parameters to be sent.
     *
     * @return array response
     */
    private function _submitHTTPGet($path, $data)
    {
        $req = $this->_encodeQS($data);
        $response = file_get_contents($path . $req);
        return $response;
    }
    /**
     * Calls the reCAPTCHA siteverify API to verify whether the user passes
     * CAPTCHA test.
     *
     * @param string $remoteIp   IP address of end user.
     * @param string $response   response string from recaptcha verification.
     *
     * @return ReCaptchaResponse
     */
    public function verifyResponse($remoteIp, $response)
    {
        // Discard empty solution submissions
        if ($response == null || strlen($response) == 0) {
            $recaptchaResponse = new ReCaptchaResponse();
            $recaptchaResponse->success = false;
            $recaptchaResponse->errorCodes = 'missing-input';
            return $recaptchaResponse;
        }
        $getResponse = $this->_submitHttpGet(
            self::$_siteVerifyUrl,
            array (
                'secret' => $this->_secret,
                'remoteip' => $remoteIp,
                'v' => self::$_version,
                'response' => $response
            )
        );
        $answers = json_decode($getResponse, true);
        $recaptchaResponse = new ReCaptchaResponse();
        if (trim($answers ['success']) == true) {
            $recaptchaResponse->success = true;
        } else {
            $recaptchaResponse->success = false;
            $recaptchaResponse->errorCodes = $answers [error-codes];
        }
        return $recaptchaResponse;
    }
}

// проверка секретного ключа


add_action('wp_ajax_my_action', 'my_action_callback');
add_action('wp_ajax_nopriv_my_action', 'my_action_callback');


function my_action_callback() {

    $secret = get_field('recaptcha_key_secret', 109);;
    $reCaptcha = new ReCaptcha($secret);


// пустой ответ
    $name = $_POST['name'];
    $mail = $_POST['mail'];
    $tel = $_POST['tel'];
    $comment = $_POST['comment'];
    $email = get_field('send_mail', 109); //get_option('admin_email');
    $response = [];
    if ($_POST["g_recaptcha_response"]) {
        $captcha_response = $reCaptcha->verifyResponse(
            $_SERVER["REMOTE_ADDR"],
            $_POST["g_recaptcha_response"]
        );
    }
    if($captcha_response === null || !$captcha_response->success){
        wp_send_json_error('Captcha error');
        return;
    }

    $fields = array(
        'required' => array(
            'name' => $name,
            'email' => $mail,
            'tel' => $tel,
        )
    );

    $response['errors'] = Validator::validate_fields($fields);

    if (count($response['errors'])) {
        wp_send_json_error($response);
        return;
    }

    $message = "Имя: $name\n" . "eMail: $mail\n" . "Телефон: $tel\n" . "Сообщение: $comment";


    $send_success = wp_mail($email, 'Сообщение с сайта The Loft - страница Контакты.', $message);

    if (!$send_success) {
        wp_send_json_error("WP_mail not send");
    }

    wp_send_json_success($response);
    die();
}



add_action('wp_ajax_get_shops', 'get_shops_callback');
add_action('wp_ajax_nopriv_get_shops', 'get_shops_callback');
function get_shops_callback(){
    $args = [
        'taxonomy' => 'shop',
        'hide_empty' => false
    ];

    if($_GET['product']){
        $shops_raw = wp_get_post_terms(intval($_GET['product']), 'shop', $args);
        $response['is_product'] = true;
    }

    else
        $shops_raw = get_terms($args);
    $cities = [];
    $shops = [];
    foreach($shops_raw as $shop){
        $cities[] = get_field('city', $shop);
        $shops[] = [
            'id' => $shop->term_id,
            'name' => $shop->name,
            'addres' => get_field('address',$shop),
            'link' => get_field('link',$shop),
            'phones' => get_field('phones',$shop),
            'lat' => get_field('lat_map',$shop),
            'lng' => get_field('lng_map',$shop),
            'city' => get_field('city',$shop),
        ];

    }
    $response['shops'] = $shops;
    $response['cities'] = $cities;
    wp_send_json_success($response);
}

