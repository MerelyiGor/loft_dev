<?php
/*
Template Name: Страница одной категории
*/
get_header();?>
    <main class="category category-table">
        <div class="breadcrumbs-wrapper">
            <ul class="breadcrumbs">
                <?php the_theme_loft_breadcrumb(); ?>
            </ul>
        </div>
        <?php
                    $category = get_queried_object();

            ?>
        <div class="category-content">
            <div class="section1">
                <div class="section1-left">
                    <h1 class="category-heading"><?= $category->name ?></h1>
                    <p class="category-description"><?= $category->description ?></p>
                </div>
                <div class="section1-right">
                    <div class="category-img" style="background-image: url(<? echo get_field('page_image', $category) ?>)"></div>
                </div>
            </div>

            <div class="section2">
                <div class="filter">
                    <div class="filter-wrapper">
                        <div class="filter-left">
                            <ul class="filter-btns">
                                <li class="filter-btn">
                                    Материал
                                    <? $materials = get_terms($args = array(
                                        'taxonomy' => 'material'
                                    )); ?>
                                    <div class="filter-items">
                                        <ul>
                                            <?foreach($materials as $material){?>
                                                <li class="filter-item">
                                                    <input type="checkbox" name="product_material" class="filter-checkbox" id="material<?= $material->term_id ?>" value="<?= $material->slug?>" />
                                                    <label for="material<?= $material->term_id ?>"><?= $material->name ?></label>
                                                </li>
                                            <?}?>
                                        </ul>
                                        <button class="filter-accept filter-submit">Применить</button>
                                    </div>
                                </li>
                                <li class="filter-btn">
                                    Тип
                                    <? $types = get_terms($args = array(
                                        'taxonomy' => 'type',
                                        'hide_empty' => false,
                                    )); ?>
                                    <div class="filter-items">
                                        <ul>
                                            <?foreach($types as $type){?>
                                                <li class="filter-item">
                                                    <input type="checkbox" name="product_type" class="filter-checkbox" id="type<?= $type->term_id ?>" value="<?= $type->slug?>" />
                                                    <label for="type<?= $type->term_id ?>"><?= $type->name ?></label>
                                                </li>
                                            <?}?>
                                        </ul>
                                        <button class="filter-accept filter-submit">Применить</button>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="category-cards">

                    <?
                        $page = isset($_GET['page']) ? $_GET['page'] : 1;
                        $per_page = 30;

                        $filter = [];
                        $filter[] = [
                            'taxonomy' => 'card_category',
                            'field' => 'slug',
                            'terms' => $category->slug];
                        if(isset($_GET['materials']))
                            $filter[] = [
                                'taxonomy' => 'material',
                                'field' => 'slug',
                                'terms' => isset($_GET['materials']) ? $_GET['materials'] : null
                            ];
                        if(isset($_GET['types']))
                            $filter[] = [
                                'taxonomy' => 'type',
                                'field' => 'slug',
                                'terms' => isset($_GET['types']) ? $_GET['types']: null
                            ];

                        $post_query = new WP_Query([
                                'post_type' => 'card',
                                 'posts_per_page' => $per_page,
                                 'paged' => $page,
                                 'tax_query' =>
                                        $filter

                        ]);

                        $posts_count = $category->count;
                        $pages_count = $posts_count / $per_page;
                        while($post_query->have_posts()):$post_query->the_post();
                        $colors = get_field('colors')?>
                    <div class="category-col">
                        <div class="category-card">
                            <div class="category-card-img">
                                <a href="#" class="card-img-big"></a>
                                <img class="card-img" src="<?= $colors[0]['product_color_image'] ?>" style="background-image: none; height:250px; object-fit: contain">
                            </div>
                            <div class="category-card-bottom">
                                <h3 class="category-card-title"><?= get_the_title(); ?></h3>
                                <div class="category-card-info">
                                    <p class="card-info-item"><span class="card-info-name">Ширина:</span><span class="card-info-val"><?= get_field('width'); ?></span></p>
                                    <p class="card-info-item"><span class="card-info-name">Глубина:</span><span class="card-info-val"><?= get_field('depth'); ?></span></p>
                                    <p class="card-info-item"><span class="card-info-name">Высота:</span><span class="card-info-val"><?= get_field('height'); ?></span></p>
                                </div>
                                <div class="category-card-footer">
                                    <div class="colors">
                                        <?php foreach ($colors as $key => $value): ?>
                                            <div class="color-border border1 product-color"><img data-image-src="<?= $value['product_color_image'] ?>" src="<?= $value['color_image'] ?>" alt="<?= $value['color'] ?>"></div>
                                        <?php endforeach ?>
                                    </div>
                                    <div class="card-price"><span><?= get_field('price'); ?></span> <sup>грн</sup></div>
                                </div>
                            </div>
                            <a href="<?php the_permalink(); // Ссылка на пост ?>" class="category-card-ref"></a>
                        </div>
                    </div>
                        <? endwhile; ?>




                </div>

                <div class="category-pagination-wrapper">
                    <div class="category-pagination">
                        <?    $page = isset($_GET['page']) ? $_GET['page'] : 1; if($page > 1) { ?>
                            <a href="?page=<?= $page - 1 ?>" class="pagination-item pagination-prev">Назад</a>
                            <?
                        }
                        $i=1;
                            while($i <= $pages_count){

                                ?>
                                <a href="?page=<?= $i ?>" class="pagination-item <? echo $i == $page ? 'current' : null ;?>"><?= $i?></a>
                                <?
                                $i++;
                            }
                        if($page < $pages_count){?>
                        <a href="?page=<?= $page + 1 ?>" class="pagination-item pagination-next">Вперед</a>
                            <?}?>
                    </div>
                </div>
<!--                <div class="category-pagination-wrapper">-->
<!--                    <div class="category-pagination">-->
<!--                        <a href="#" class="pagination-item pagination-prev">Назад</a>-->
<!--                        <a href="#" class="pagination-item">1</a>-->
<!--                        <a href="#" class="pagination-item current">2</a>-->
<!--                        <a class="pagination-item pagination-disabled">...</a>-->
<!--                        <a href="#" class="pagination-item">4</a>-->
<!--                        <a href="#" class="pagination-item pagination-next">Вперед</a>-->
<!--                    </div>-->
<!--                </div>-->
            </div>
            <?php wp_reset_postdata(); ?>
            <div class="section3">
                <div class="category-text-block"><?= get_field('text',$category); ?></div>
                <div class="section3-bg-text"><?= $category->name ?></div>
            </div>
        </div>
    </main>
    <div class="home">
        <div class="popup-s4">
            <img class="customimg" data-image="my-image" src="" alt="">
            <div class="close-popup">
                <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 21 21">
                    <path  fill-rule="evenodd" d="M12.621 10.701l7.778 7.778-2.12 2.122-7.779-7.779-7.778 7.779L.6 18.479l7.778-7.778L.6 2.923 2.72.802 10.5 8.58 18.278.802l2.121 2.12-7.778 7.779z"/>
                </svg>
            </div>
        </div>
    </div>
<?php
get_footer();
