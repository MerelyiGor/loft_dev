
// Подключить нужно только после vendor или jquery

$('.contacts form').submit(function(event){
    event.preventDefault();

    var form = $(this),
        data =  {
            action: 'my_action', //название action который зарегистрирован для обработки данных в php
            name: form.find('input[name="name"]').val(), //берутся значения по input и атрибуту name
            mail: form.find('input[name="email"]').val(),
            tel: form.find('input[name="tel"]').val(),
            comment: form.find('textarea[name="comment"]').val(),
            g_recaptcha_response: form.find('textarea[name="g-recaptcha-response"]').val()

        };
    console.table(data);
    $.post( myajax.url, data, function(response) {
        console.table(response);
        form.get(0).reset();
        grecaptcha.reset()
    });

});
var substringMatcher = function(strs) {
    return function findMatches(q, cb) {
        var matches, substringRegex;

        // an array that will be populated with substring matches
        matches = [];

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, 'i');

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        $.each(strs, function(i, str) {
            if (substrRegex.test(str)) {
                matches.push(str);
            }
        });

        cb(matches);
    };
};
var shops_data = {
  action : 'get_shops',
  product : window.product_id !== undefined ? window.product_id : null
};
console.log(shops_data);
$.get(myajax.url, shops_data, function(response){
    console.log(response);
    window.cities = Array.from(new Set(response.data.cities));
    window.shops = response.data.shops;
    $('.typeahead').typeahead({
        minLength: 1,
        highlight: true,
        hint: true,
        limit: 10,
    },
    {
        name: 'cities',
        source: substringMatcher(window.cities)
    });
    if(response.data.is_product){
        var root = $('.result__search .body');
        root.html('');
        window.shops.forEach(function(shop){
            renderShop(root, shop);
        });
        root.parent().addClass('active');
    }
});
window.mapMarkers = new Object;

$('.cities-search').on('typeahead:select',function(){
    console.log($(this).val());
    var city = $(this).val();
    var selected_shops = window.shops.filter(shop => shop.city === city);
    console.log(selected_shops);
    var root = $('.result__search .body');
    root.html('');
    selected_shops.forEach(function(shop){
        renderShop(root, shop);
    });
    root.parent().addClass('active');

});
function renderShop(root, shop){
    var phones = '';
    shop.phones.forEach(function(el){
        phones += "<a href=\"tel:" + el.phone + "\">" + el.phone + "</a>\n";
    });
    var element = $("<div data-id=\""+ shop.id +"\" class=\"result\">\n" +
        "                    <div class=\"result_top\">\n" +
        "                        <div class=\"result__left\">\n" +
        "                            <span class=\"span-fix-search text-result-shop\">" + shop.name + "</span>\n" +
        "                        </div>\n" +
        "                        <div class=\"result__right\">\n" +
                                    phones +
        "                        </div>\n" +
        "                    </div>\n" +
        "                    <div class=\"result_bottom\">\n" +
        "                        <div class=\"result__left\">\n" +
        "                            <span class=\"span-fix-search\">" + shop.addres + "</span>\n" +
        "                        </div>\n" +
        "                        <div class=\"result__right\">\n" +
        "                            <a target='_blank' rel='nofollow' class=\"link\" href=\"" + shop.link + "\">" + shop.link + "</a>\n" +
        "                        </div>\n" +
        "                    </div>\n" +
        "                </div>");

    element.appendTo(root);
    window.mapMarkers[shop.id] = new google.maps.Marker(
        {
            map: window.map,
            title: shop.name,
            icon: window.mapMarkerImage,

        });
    window.mapMarkers[shop.id].setPosition(new google.maps.LatLng(parseFloat(shop.lat), parseFloat(shop.lng)));
    window.map.setCenter(window.mapMarkers[shop.id].getPosition());
};

$('body').on('click', '.result', function(){
    var id = $(this).data('id');
    var shop = window.shops.filter(shop => shop.id === id)[0];
    window.map.setCenter(window.mapMarkers[id].getPosition());
    window.mapMarkers[id].setIcon(window.mapMarkerImageActive)

    infowindow = new google.maps.InfoWindow({
    content: '<p id="map-hook">' + shop.name + '</p>',
    });
    infowindow.open(window.map,  window.mapMarkers[id] );
    google.maps.event.addListener(infowindow, 'domready', function() {
        var l = $('#map-hook').parent().parent().parent().siblings();
        var root = $('#map-hook').parent().parent().parent();
        for (var i = 0; i < l.length; i++) {
            if($(l[i]).css('z-index') == 'auto') {
                $(l[i]).css('box-shadow', '-1px 19px 42px 0 rgba(29, 32, 108, 0.13)');
                $(l[i]).css('padding', '20px 30px');
                $(l[i]).css('border-radius', '0');

            }
        }
    });



});
$('.product-color img').click(function () {
    $(this).parents('.category-card').find('.card-img').attr('src',$(this).data('image-src'));
});
$('.card-img-big').click(function(e){
    e.preventDefault();
    $('.customimg').attr('src',$(this).next().attr('src'));
    $('.popup-s4').addClass('popup-open');
});

$('.card-img-big-single').click(function(e){
    e.preventDefault();
    $('.customimg').attr('src',$('.slick-current img').data('image-src'));
    $('.popup-s4').addClass('popup-open');
});

function insertParam(key, value) {
    key = encodeURI(key);
    value = encodeURI(value);
    return key + '=' + value;
}

    //this will reload the page, it's likely better to store this until finished


$('.filter-submit').click(function(){
    var kvp = [];
   $('input[name="product_material"]:checked').each(function(item) {
       kvp.push(insertParam('materials[]', $(this).val())) ;
    });
    $('input[name="product_type"]:checked').each(function(item) {
        kvp.push(insertParam('types[]', $(this).val())) ;
    });
    document.location.search = kvp.join('&');
    console.log(kvp);
});