<?php
get_header(); the_post()?>


<main class="card">

<div class="breadcrumbs-wrapper">
    <ul class="breadcrumbs">
        <?= the_theme_loft_breadcrumb() ?>
    </ul>
</div>

<div class="section1">
    <div class="slider">
        <div class="slider-big">
            <? $colors = get_field('colors'); foreach($colors as $key => $value){ ?>
                <div class="slick-slide"><img data-image-src="<?= $value['product_color_image'] ?>" src="<?= $value['product_color_image'] ?>" alt=""></div>
           <? } ?>
  
        </div>
        <div class="slider-litl">
            <? foreach($colors as $key => $value){ ?>
                <div class="slick-slide"><img src="<?= $value['product_color_image'] ?>" alt=""></div>
            <? } ?>
    
        </div>
        <div class="full-screen ">



            <a href="#" class="card-img-big-single">

                <svg xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                    <path fill-rule="evenodd" d="M10.8 24.3H4.609l6.191-6.191L8.891 16.2 2.7 22.391V16.2H0V27h10.8v-2.7zM27 0H16.2v2.7h6.191L16.25 8.841l1.909 1.91L24.3 4.608V10.8H27V0z"/>
                </svg>
            </a>



        </div>
        <div class="share">
            <svg xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                <path fill-rule="evenodd" d="M21.6 16.031c-1.489 0-2.7-1.21-2.7-2.7 0-1.489 1.211-2.7 2.7-2.7s2.7 1.211 2.7 2.7c0 1.49-1.211 2.7-2.7 2.7M5.4 24.3c-1.49 0-2.7-1.211-2.7-2.7s1.21-2.7 2.7-2.7c3.568 0 3.57 5.4 0 5.4m0-16.2a2.703 2.703 0 0 1-2.7-2.7c0-1.49 1.21-2.7 2.7-2.7 3.568 0 3.57 5.4 0 5.4m16.2-.169c-1.648 0-3.105.755-4.096 1.919l-6.76-3.903C11.087 2.59 8.465 0 5.4 0a5.4 5.4 0 0 0 0 10.8 5.385 5.385 0 0 0 4.397-2.283l6.51 3.759a5.317 5.317 0 0 0 .012 2.174L9.64 18.294C8.652 17.028 7.13 16.2 5.4 16.2a5.4 5.4 0 0 0 0 10.8c3.189 0 5.837-2.797 5.318-6.211l6.825-3.928c.99 1.137 2.432 1.87 4.057 1.87a5.4 5.4 0 0 0 0-10.8"/>
            </svg>
        </div>
    </div>
    <div class="r-block">
        <a class="s1-btn mob" >Где купить</a>
        <div class="pre-titl"><?= wp_get_post_terms(get_the_ID(), ['taxonomy'=>'card_category'])[0]->name; ?></div>
        <div class="titl"><?= get_the_title(); ?></div>
        <div class="switching-color">
            <div class="set">
                <p>Цвет :</p>
                <div class="color-name color-name-active"><?php echo $colors[0]['color']?></div>
                
            </div>
            <div class="colors">
                    <?php $sliderCount = 0;foreach ($colors as $key => $value): ?>
                        <div class="color-border"><img data-name="<?php echo $value['color']?>" data-slide="<?= $sliderCount++ ?>"src="<?= $value['color_image'] ?>" data-id="<?= $key ?>"alt=""></div>
                    <?php endforeach ?>
            </div>
        </div>
        <div class="price">
            <p class="big"><?= get_field('price'); ?></p>
            <p class="litl">грн</p>
        </div>
        <? $types = wp_get_post_terms(get_the_ID(), 'type');
        $materials = wp_get_post_terms(get_the_ID(), 'material');
        $category = wp_get_post_terms(get_the_ID(), 'card_category');
         ?>
        <a href="/?buy_product_id=<?= get_the_ID(); ?>/#section5" class="s1-btn" >Где купить</a>
        <div class="characteristics">
            <div class="titl-char">Характеристики</div>
            <ul>
                <li><p>Производитель:<span><?= get_field('manufacturer')?></span> </p></li>
                <li><p>Тип мебели:<span><?= $category[0]->name ?></span> </p></li>
                <li><p>Материал:<span><?= $materials[0]->name ?></span> </p></li>
                <li><p> Тип:<span><?= $types[0]->name ?></span> </p></li>
            </ul>
            <ul class="r-ul">
                <li><p> Ширина:<span><?= get_field('width'); ?></span> </p></li>
                <li><p> Высота:<span><?= get_field('depth'); ?></span> </p></li>
                <li><p> Глубина:<span><?= get_field('height'); ?></span> </p></li>
            </ul>
        </div>
    </div>
</div>

<div class="section2">
    <div class="l-block">
        <div class="titl">Цветовая гамма:</div>
        <div class="img-gamma">
            <ul>
                <?php foreach ($colors as $key => $value): ?>
                    <li><img data-image-src="<?= $value['product_color_image'] ?>" src="<?= $value['color_image'] ?>" data-id="<?= $key ?>"alt=""></li>
                <?php endforeach ?>
                
            </ul>
        </div>
        <div class="l-bottom">
            <div class="titl-bot">Материалы используемые в нашей продукции:</div>
            <ul>
                <li>
                    <img src="<?= get_template_directory_uri() ?>/media/img/card/wood.png" alt="">
                    <p><?= get_field('first_material') ?> </p>
                </li>
                <li>
                    <img src="<?= get_template_directory_uri() ?>/media/img/card/compass.png" alt="">
                    <p><?= get_field('second_material') ?></p>
                </li>
                <li>
                    <img src="<?= get_template_directory_uri() ?>/media/img/card/wrench.png" alt="">
                    <p><?= get_field('third_material') ?></p>
                </li>
            </ul>
        </div>
    </div>
    <div class="r-block">
        <div class="titl">Описание</div>
        <div class="text"><? the_content() ?>
        </div>
        <div class="button">
            <a class="description hide" >Скрыть описание</a>
            <a class="description read" >Прочитать описание</a>
            <a class="buy" href="/?buy_product_id=<?= get_the_ID(); ?>/#section5" >Где купить</a>
        </div>
        <div class="r-bottom">
            <div class="titl-bot">Купить Письменный стол <? the_title() ?></div>
            <div class="partner">
                <p>Вы можете заказать этот товар «<? the_title() ?>»
                    у наших партнёров, которые продают нашу мебель</p>
                <a href="/#section5" >Наши партнеры</a>
            </div>
        </div>
    </div>
</div>
<div class="section2 mob-s2">
    <div class="r-block">
        <div class="titl">Описание</div>
        <div class="text"><? the_content() ?>
        </div>
        <div class="button">
            <a class="description hide" >Скрыть описание</a>
            <a class="description read" >Прочитать описание</a>
        </div>
    </div>
    <div class="l-block">
        <div class="l-bottom">
            <div class="titl-bot">Материалы используемые в нашей продукции:</div>
            <ul>
                <li>
                    <img src="<?= get_template_directory_uri() ?>/media/img/card/wood.png" alt="">
                    <p><?= get_field('first_material') ?> </p>
                </li>
                <li>
                    <img src="<?= get_template_directory_uri() ?>/media/img/card/compass.png" alt="">
                    <p><?= get_field('second_material') ?></p>
                </li>
                <li>
                    <img src="<?= get_template_directory_uri() ?>/media/img/card/wrench.png" alt="">
                    <p><?= get_field('third_material') ?></p>
                </li>
            </ul>
        </div>
        <div class="titl">Цветовая гамма:</div>
        <div class="img-gamma">
            <ul>
            <?php foreach ($colors as $key => $value): ?>
                    <li><img data-image-src="<?= $value['product_color_image'] ?>" src="<?= $value['color_image'] ?>" data-id="<?= $key ?>"alt=""></li>
                <?php endforeach ?>
            </ul>
        
        </div>
    </div>
    <div class="r-bottom">
        <div class="titl-bot">Купить Письменный стол <? the_title() ?></div>
        <div class="partner">
            <p>Вы можете заказать этот товар «<?= get_the_title(); ?>>»
                у наших партнёров, которые продают нашу мебель</p>
            <a href="/#section5" >Наши партнеры</a>
        </div>
    </div>
</div>

<div class="bg-card"><? the_title() ?></div>


    <div class="home">
        <div class="popup-s4">
            <img class="customimg" data-image="my-image" src="" alt="">
            <div class="close-popup">
                <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 21 21">
                    <path  fill-rule="evenodd" d="M12.621 10.701l7.778 7.778-2.12 2.122-7.779-7.779-7.778 7.779L.6 18.479l7.778-7.778L.6 2.923 2.72.802 10.5 8.58 18.278.802l2.121 2.12-7.778 7.779z"/>
                </svg>
            </div>
        </div>
    </div>



</main>
<? get_footer(); ?>