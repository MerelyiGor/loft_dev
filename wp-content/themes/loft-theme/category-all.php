<?php
/*
Template Name: Категории товаров
*/
get_header();?>
    <main class="category category-plus">
        <div class="breadcrumbs-wrapper">
            <ul class="breadcrumbs">
                <?php the_theme_loft_breadcrumb();?>
<!--                <li class="breadcrumbs-item"><a href="index.html">Главная</a></li>-->
<!--                <li class="breadcrumbs-item breadcrumbs-shortcut"><a href="#">Сотрудничество</a></li>-->
<!--                <li class="breadcrumbs-item breadcrumbs-current"><a>Дилерам</a></li>-->

            </ul>
        </div>
        <div class="category-content">
            <div class="section1">
                <div class="section1-left">
                    <h1 class="category-heading">Каталог</h1>
                    <p class="category-description"><?= get_field('subtitle'); ?></p>
                </div>
                <div class="section1-right">
                    <div class="category-img" style="background-image: url(<?= get_field('page_img'); ?>)"></div>
                </div>
            </div>

            <div class="section2">
                <div class="category-goods">
                    <?php
                    $categories = get_terms($args = array(
                        'taxonomy' => 'card_category',
                        'hide_empty' => false,
                    ));
                    function bigCard($category){
                        return '<div class="category-goods-col"><div class="category-goods-item category-goods-big"><div class="category-goods-img">
                        <img src="'.get_field('catalog_image', $category).'" alt="' . sprintf( __( "изображение из  %s" ), $category->name ) . '">
                        </div>
                        <a href="' . get_category_link( $category->term_id ) . '" class="category-goods-link"></a>
                        <a href="' . get_category_link( $category->term_id ) . '" class="category-goods-name">' . $category->name.'</a>
                       </div></div>';
                    }
                    function columnCard($category1, $category2){
                      return '<div class="category-col-wrapper">
                        <div class="category-goods-col"><div class="category-goods-item"><div class="category-goods-img">
                        <img id="fix-width-card" src="'.get_field('catalog_image', $category1).'" alt="' . sprintf( __( "изображение из  %s" ), $category1->name ) . '">
                        </div>
                        <a href="' . get_category_link( $category1->term_id ) . '" class="category-goods-link"></a>
                        <a href="' . get_category_link( $category1->term_id ) . '" class="category-goods-name">' . $category1->name.'</a>
                        </div></div>
                        <div class="category-goods-col"><div class="category-goods-item"><div class="category-goods-img">
                        <img id="fix-width-card" src="'.get_field('catalog_image', $category2).'" alt="' . sprintf( __( "изображение из  %s" ), $category2->name ) . '">
                        </div>
                        <a href="' . get_category_link( $category2->term_id ) . '" class="category-goods-link"></a>
                        <a href="' . get_category_link( $category2->term_id ) . '" class="category-goods-name">' . $category2->name.'</a>
                        </div></div>
                       </div>';

                    }
                    function smallCard($category){
                        return '<div class="category-col-wrapper category-col-wrapper-res"><div class="category-goods-item"><div class="category-goods-img">
                        <img src="'.get_field('catalog_image', $category).'" alt="' . sprintf( __( "изображение из  %s" ), $category->name ) . '">
                        </div>
                        <a href="' . get_category_link( $category->term_id ) . '" class="category-goods-link"></a>
                        <a href="' . get_category_link( $category->term_id ) . '" class="category-goods-name">' . $category->name.'</a>
                        </div></div>';
                    }
                    echo bigCard($categories[0]);
                    echo bigCard($categories[1]);
                    echo columnCard($categories[2], $categories[3]);
                    $n = 4;
                    $rowCounter = 0;
                    while($n < count($categories)){
                        echo '<div class="category-row-wrapper">';
                        while($rowCounter < 3){
                            if(isset($categories[$n + $rowCounter]))
                                echo smallCard($categories[$n + $rowCounter]);
                            $rowCounter++;
                        }
                        echo '</div>';
                        $rowCounter = 0;
                        $n += 3;
                    }
                    ?>



                </div>
            </div>
        </div>
    </main>

<?php
get_footer();