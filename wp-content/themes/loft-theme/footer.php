<footer class="footer">
    <?php
    if( is_404() ){
        ?>
<!--        <div class="bot-inf">-->
<!--            <div class="year">© 2018 The loft, Inc.</div>-->
<!--            <div class="maid">Сделано в <span>Polyarix</span></div>-->
<!--        </div>-->
        <?php
    } else{
       ?>
        <div class="l-block">
            <ul class="social">
                <li class="soc">
                    <a href="<?= get_field('googleplus',2); ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26">
                            <path  fill-rule="evenodd" d="M25.768 10.405H13.275c0 1.3 0 3.898-.008 5.197h7.24c-.278 1.3-1.262 3.118-2.652 4.034 0-.001-.002.008-.005.007-1.848 1.22-4.286 1.497-6.096 1.133-2.838-.564-5.084-2.622-5.996-5.236.005-.004.009-.04.013-.043-.57-1.622-.57-3.793 0-5.092H5.77c.735-2.388 3.049-4.567 5.89-5.163 2.286-.485 4.865.04 6.762 1.815.252-.247 3.491-3.41 3.734-3.667C15.676-2.479 5.3-.414 1.417 7.165h-.001l-.007.014c-1.92 3.722-1.84 8.109.014 11.653l-.014.01a13.234 13.234 0 0 0 8.424 6.715c3.914 1.027 8.896.325 12.233-2.693l.004.004c2.827-2.547 4.587-6.885 3.698-12.463"/>
                        </svg>
                    </a>
                </li>
                <li class="soc twit">
                    <a href="<?= get_field('twiter',2); ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" width="26" height="21" viewBox="0 0 26 21">
                            <path  fill-rule="evenodd" d="M8.177 21c9.811 0 15.177-8.08 15.177-15.087 0-.23 0-.458-.015-.686A10.8 10.8 0 0 0 26 2.483c-.972.429-2.006.71-3.063.834A5.332 5.332 0 0 0 25.282.384a10.73 10.73 0 0 1-3.387 1.287 5.363 5.363 0 0 0-7.548-.23 5.29 5.29 0 0 0-1.543 5.067A15.182 15.182 0 0 1 1.81.968a5.287 5.287 0 0 0 1.652 7.078 5.319 5.319 0 0 1-2.422-.663v.067a5.314 5.314 0 0 0 4.28 5.197 5.338 5.338 0 0 1-2.408.09 5.335 5.335 0 0 0 4.983 3.683A10.743 10.743 0 0 1 0 18.618a15.173 15.173 0 0 0 8.177 2.378"/>
                        </svg>
                    </a>
                </li>
                <li class="soc facebook">
                    <a href="<?= get_field('facebook',2); ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="26" viewBox="0 0 13 26">
                            <path  fill-rule="evenodd" d="M8.868 26V14.3h3.552L13 9.1H8.868V6.568c0-1.34.034-2.668 1.905-2.668h1.895V.182C12.668.126 11.04 0 9.393 0 5.954 0 3.8 2.154 3.8 6.11V9.1H0v5.2h3.8V26h5.068z"/>
                        </svg>
                    </a>
                </li>
                <li class="soc">
                    <a href="<?= get_field('instagram',2); ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" width="26" height="26" viewBox="0 0 26 26">
                            <path fill-rule="evenodd" d="M7.63.16C5.515.255 3.68.772 2.2 2.247.713 3.73.202 5.571.106 7.665c-.06 1.308-.408 11.183.601 13.772a6.553 6.553 0 0 0 3.784 3.773c.823.32 1.762.537 3.14.6 11.519.522 15.789.238 17.59-4.373.319-.82.539-1.758.6-3.133.526-11.548-.086-14.051-2.094-16.057C22.135.66 20.262-.423 7.631.16m.106 23.328c-1.262-.057-1.946-.267-2.403-.444a4.231 4.231 0 0 1-2.455-2.448c-.768-1.968-.513-11.314-.445-12.826.068-1.481.368-2.835 1.413-3.88C5.14 2.6 6.812 1.967 18.19 2.48c1.485.068 2.842.367 3.89 1.41 1.293 1.29 1.935 2.974 1.412 14.31-.057 1.258-.267 1.94-.445 2.396-1.17 3-3.863 3.417-15.311 2.892m10.58-17.392a1.55 1.55 0 0 0 1.552 1.549c.857 0 1.553-.694 1.553-1.549 0-.854-.696-1.548-1.553-1.548a1.55 1.55 0 0 0-1.552 1.548M6.32 12.984c0 3.66 2.974 6.626 6.643 6.626 3.668 0 6.642-2.966 6.642-6.626S16.632 6.36 12.964 6.36c-3.669 0-6.643 2.965-6.643 6.624m2.331 0c0-2.374 1.93-4.3 4.312-4.3a4.307 4.307 0 0 1 4.311 4.3 4.307 4.307 0 0 1-4.311 4.302 4.307 4.307 0 0 1-4.312-4.302"/>
                        </svg>
                    </a>
                </li>
                <li class="soc">
                    <a href="<?= get_field('youtube',2); ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" width="26" height="18" viewBox="0 0 26 18">
                            <path fill-rule="evenodd" d="M10.385 12.325V5.11c2.59 1.205 4.596 2.37 6.968 3.625-1.957 1.073-4.378 2.277-6.968 3.59M24.818 1.52C24.372.939 23.61.486 22.8.336 20.417-.11 5.552-.113 3.17.336c-.65.12-1.228.412-1.725.864C-.65 3.124.006 13.438.51 15.108c.212.724.487 1.245.833 1.588.445.452 1.055.764 1.756.904 1.961.401 12.068.625 19.658.06.7-.12 1.318-.442 1.806-.914 1.938-1.916 1.806-12.81.254-15.225"/>
                        </svg>
                    </a>
                </li>
            </ul>
            <?php include(TEMPLATEPATH . '/part/subscribe.php'); ?>
        </div>
        <div class="r-block">
            <?php wp_nav_menu('menu_class=bmenu&theme_location=bleft'); ?>
            <?php wp_nav_menu('menu_class=bmenu&theme_location=bcenter'); ?>
            <?php wp_nav_menu('menu_class=contakt&theme_location=bright'); ?>
            <?php include(TEMPLATEPATH . '/part/svg/footer-logo.php'); ?>
        </div>
        <div class="bot-inf">
            <div class="year">© 2018 The loft, Inc.</div>
            <div class="maid">Сделано в <span><a href="https://polyarix.com">Polyarix</a></span></div>
        </div>
        <?php
    }?>
    <?php include(TEMPLATEPATH . '/part/main-map-search.php'); // отвечает за скрипты карты на главной и маркеры с выводом?>
    <?php
    if ( is_page(109)) {
        ?>

        <script>
          function initMap() {
            var centerLatLng = new google.maps.LatLng(50.463696, 30.513968);
            // Обязательные опции с которыми будет проинициализированна карта
            var mapOptions = {
                center: centerLatLng, // Координаты центра мы берем из переменной centerLatLng
                zoom: 11,               // Зум по умолчанию. Возможные значения от 0 до 21
                styles:[
                    { "featureType": "all", "elementType": "labels.text.fill",
                        "stylers": [
                            {"saturation": 36},
                            {"color": "#333333"},
                            {"lightness": 40}
                        ]
                    },
                    {"featureType": "all","elementType": "labels.text.stroke",
                        "stylers": [
                            {"visibility": "on"},
                            {"color": "#ffffff"},
                            {"lightness": 16}
                        ]
                    },
                    {"featureType": "all","elementType": "labels.icon",
                        "stylers": [
                            {"visibility": "off"}
                        ]
                    },
                    {"featureType": "administrative","elementType": "geometry.fill",
                        "stylers": [
                            {"color": "#f7f7f7"},
                            {"lightness": 20}
                        ]
                    },
                    {"featureType": "administrative","elementType": "geometry.stroke",
                        "stylers": [
                            {"color": "#f7f7f7"},
                            {"lightness": 17},
                            {"weight": 1.2}
                        ]
                    },
                    {"featureType": "landscape","elementType": "geometry",
                        "stylers": [
                            {"color": "#f7f7f7"},
                            {"lightness": 20}
                        ]
                    },
                    { "featureType": "poi","elementType": "geometry",
                        "stylers": [
                            {"color": "#f5f5f5"},
                            {"lightness": 21}
                        ]
                    },
                    {"featureType": "poi.park","elementType": "geometry",
                        "stylers": [
                            {"color": "#dedede"},
                            {"lightness": 21}
                        ]
                    },
                    {"featureType": "road.highway","elementType": "geometry.fill",
                        "stylers": [
                            {"color": "#ffffff"},
                            {"lightness": 17}
                        ]
                    },
                    {"featureType": "road.highway","elementType": "geometry.stroke",
                        "stylers": [
                            { "color": "#ffffff"},
                            {"lightness": 29},
                            {"weight": 0.2}
                        ]
                    },
                    {"featureType": "road.arterial","elementType": "geometry",
                        "stylers": [
                            {"color": "#ffffff"},
                            {"lightness": 18}
                        ]
                    },
                    {"featureType": "road.local","elementType": "geometry",
                        "stylers": [
                            {"color": "#ffffff"},
                            {"lightness": 16}
                        ]
                    },
                    {"featureType": "transit","elementType": "geometry",
                        "stylers": [
                            {"color": "#f2f2f2"},
                            {"lightness": 19}
                        ]
                    },
                    {"featureType": "water","elementType": "geometry",
                        "stylers": [
                            {"color": "#99ccc0"},
                            {"lightness": 17}
                        ]
                    }
                ]
            };
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);
        }
        </script>
        <script async defer  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAb0QGeFu5sD7h3P_MeHoGrmUZqT97UOSk&callback=initMap"type="text/javascript"></script>
        <?
    }
    ?>


</footer>

<?php wp_footer(); ?>
</body>
</html>
