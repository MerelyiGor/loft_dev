<?php
/**
 * The front page example template file
 * ---------------------------------------------------------------------------------------------------------------------
 **/
get_header(); ?>

    <main class="home">
        <div class="popup-s4">
            <img class="customimg" data-image="my-image" src="" alt="">
            <div class="close-popup">
                <svg xmlns="http://www.w3.org/2000/svg" width="21" height="21" viewBox="0 0 21 21">
                    <path  fill-rule="evenodd" d="M12.621 10.701l7.778 7.778-2.12 2.122-7.779-7.779-7.778 7.779L.6 18.479l7.778-7.778L.6 2.923 2.72.802 10.5 8.58 18.278.802l2.121 2.12-7.778 7.779z"/>
                </svg>
            </div>
        </div>
        <div class="section1">
            <div class="swiper-container swiper-sl1">
                <div class="swiper-wrapper">

                    <?php foreach (get_field('main_slider') as $key => $value): ?>
                        <!-- Slides -->
                        <div class="swiper-slide">
                            <div class="text-center">
                                <div class="titl titl-bg"><?= $value['ttl_slide'] ?></div>
                                <p><?= $value['txt_slide'] ?></p>
                            </div>
                            <div class="img">
                                <img src="<?= $value['txt_slide_card_img'] ?>" alt="">
                                <p class="s1-pop-up"><?= $value['txt_slide_card'] ?></p>
                                <div class="s1-plus-up">
                                    <div class="img-w">
                                        <div class="img s1-plus-img">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 8 8">
                                                <path fill-rule="evenodd" d="M8 3v2H5v3H3V5H0V3h3V0h2v3z"/>
                                            </svg>
                                        </div>
                                        <div class="img s1-minus-img">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="8" height="2" viewBox="0 0 8 2">
                                                <path fill-rule="evenodd" d="M8 0v2H0V0h3z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>

                </div>
                <!-- If we need pagination -->
                <div class="swiper-pagination sp1 desc"></div>
                <!--<div class="swiper-scrollbar ss1"></div>-->
            </div>
            <a href="#section2" class="bot-anim">
                <p>Листай ниже </p>
                <div class="mous"><span></span></div>
            </a>
        </div>

        <div id="section2" class="section2">

            <div class="l-block l-command">
                <img class="s2-comm-img" src="<?= get_field('img-comnash'); ?>" alt="">
                <img class="s2-hist-img" src="<?= get_field('img-history'); ?>" alt="">
                <img class="s2-achiev-img" src="<?= get_field('img-dostij'); ?>" alt="">
                <p class="s2-comm-p">Наша команда</p>
                <p class="s2-hist-p">История</p>
                <p class="s2-achiev-p">Достижения</p>
            </div>

            <div class="r-block">
                <div class="titl"><?= get_field('title-block1'); ?></div>
                <ul>
                    <li class="command-b activ-s2-b"><a>Наша команда</a></li>
                    <li class="history-b"><a>История</a></li>
                    <li class="achievements-b"><a>Достижения</a></li>
                </ul>
                <div class="lin"></div>
            </div>
            <div class="bot-block">
                <div class="command bot-text activ-s2">
                    <div class="titl">Наша Kоманда</div>
                    <p><?= get_field('tx_hjbv-1'); ?></p>
                </div>
                <div class="history bot-text">
                    <div class="titl">История</div>
                    <p><?= get_field('tx_hjbv-2'); ?></p>
                </div>
                <div class="achievements bot-text">
                    <div class="titl">Достижения</div>
                    <p><?= get_field('tx_hjbv-3'); ?></p>
                </div>

                <a href="<?= get_field('url_sevibu'); ?>"> Подробнее
                    <svg xmlns="http://www.w3.org/2000/svg" width="28" height="15" viewBox="0 0 28 15">
                        <path fill-rule="evenodd" d="M28 7l-8-7-2 2 5 4H0v2h23l-5 5 2 2z"/>
                    </svg>
                </a>
            </div>
            <div class="bg-paralax" id="bg-parallax-1">О КОМ</div>
        </div>

        <div id="section3" class="section3">
            <div class="l-block">
                <div class="titl"><?= get_field('title-block-swckm'); ?></div>
                <ul>
                    <li class="Curbstones-b activ-s3-b"><a> Офисные Тумбы </a></li>
                    <li class="tables-b"><a> Офисные столы </a></li>
                    <li class="racks-b"><a> Офисные стелажи </a></li>
                    <li class="Coffee-tables-b"><a> Журнальные столики </a></li>
                </ul>
                <div class="lin"></div>
            </div>
            <div class="r-block">
                <img class="s3-img1 activ-s3-img" src="<?= get_field('img-sdvo-1'); ?>" alt="">
                <img class="s3-img2" src="<?= get_field('img-sdvo-2'); ?>" alt="">
                <img class="s3-img3" src="<?= get_field('img-sdvo-3'); ?>" alt="">
                <img class="s3-img4" src="<?= get_field('img-sdvo-4'); ?>" alt="">
                <p class="s3-pop-up up1 up-active-s3"><?= get_field('loftdesignlmini-1'); ?></p>
                <p class="s3-pop-up up2"><?= get_field('loftdesignlmini-4'); ?></p>
                <p class="s3-pop-up up3"><?= get_field('loftdesignlmini-3'); ?></p>
                <p class="s3-pop-up up4"><?= get_field('loftdesignlmini-2'); ?></p>
                <div class="s3-plus-up">
                    <div class="img-s3">
                        <div class="img s3-plus-img">
                            <svg xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 8 8">
                                <path fill-rule="evenodd" d="M8 3v2H5v3H3V5H0V3h3V0h2v3z"/>
                            </svg>
                        </div>
                        <div class="img s3-minus-img">
                            <svg xmlns="http://www.w3.org/2000/svg" width="8" height="2" viewBox="0 0 8 2">
                                <path fill-rule="evenodd" d="M8 0v2H0V0h3z"/>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bot-block">
                <div class="Curbstones bot-text ">
                    <div class="titl">Офисные Тумбы</div>
                    <p><?= get_field('lofttxtitxt-1'); ?></p>
                </div>
                <div class="tables bot-text activ-s3">
                    <div class="titl">Офисные столы</div>
                    <p><?= get_field('lofttxtitxt-2'); ?></p>
                </div>
                <div class="racks bot-text">
                    <div class="titl">Офисные стелажи</div>
                    <p><?= get_field('lofttxtitxt-3'); ?></p>
                </div>
                <div class="Coffee-tables bot-text">
                    <div class="titl">Журнальные столики</div>
                    <p><?= get_field('lofttxtitxt-4'); ?></p>
                </div>
                <a href="<?= get_field('url-fkl'); ?>"> В каталог </a>
            </div>
            <div class="bg-paralax" id="bg-parallax-2">КАТАЛОГ</div>
        </div>

        <div id="section4" class="section4">
            <div class="titl">ОТЗЫВЫ</div>
            <div class="swiper-container swiper-sl2">
                <div class="sw-w"><p>ОТЗЫВЫ</p></div>
                <div class="swiper-wrapper">
                    <?php $p = 'plus-inf'; $i = 'inf-sl'; $n = 1; foreach (get_field('review') as $key => $value): ?>
                        <!-- Slides -->
                        <div class="swiper-slide">
                            <div class="center-block">
                                <img class="photo" src="<?= $value['img'] ?>" alt="">
                                <div class="name"><?= $value['name'] ?></div>
                                <svg viewBox="0 0 82 75" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <!-- Generator: Sketch 49 (51002) - http://www.bohemiancoding.com/sketch -->
                                    <title>“</title>
                                    <desc>Created with Sketch.</desc>
                                    <defs></defs>
                                    <g id="homepage-1920" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                                       opacity="0.5">
                                        <g id="loft-homepag-1920-" transform="translate(-672.000000, -3448.000000)"
                                           fill="#E6E6E6">
                                            <path d="M700.476364,3496.94578 C702.364858,3499.65664 703.309091,3502.91966 703.309091,3506.73494 C703.309091,3511.55424 701.818197,3515.46986 698.836364,3518.48193 C695.854531,3521.49399 692.176992,3523 687.803636,3523 C683.032703,3523 679.206075,3521.44379 676.323636,3518.33133 C673.441198,3515.21886 672,3511.35344 672,3506.73494 C672,3504.32529 672.248482,3501.96587 672.745455,3499.65663 C673.242427,3497.34738 674.286053,3493.98396 675.876364,3489.56627 L688.4,3448 L702.712727,3448 L692.574545,3491.37349 C695.953956,3492.37752 698.587869,3494.23493 700.476364,3496.94578 Z M751.167273,3496.94578 C753.055767,3499.65664 754,3502.91966 754,3506.73494 C754,3511.55424 752.509106,3515.46986 749.527273,3518.48193 C746.54544,3521.49399 742.867901,3523 738.494545,3523 C733.723613,3523 729.896984,3521.44379 727.014545,3518.33133 C724.132107,3515.21886 722.690909,3511.35344 722.690909,3506.73494 C722.690909,3504.32529 722.939391,3501.96587 723.436364,3499.65663 C723.933336,3497.34738 724.976962,3493.98396 726.567273,3489.56627 L739.090909,3448 L753.403636,3448 L743.265455,3491.37349 C746.644865,3492.37752 749.278778,3494.23493 751.167273,3496.94578 Z"
                                                  id="“"></path>
                                        </g>
                                    </g>
                                </svg>
                                <div class="position"><?= $value['position'] ?></div>
                                <p><?=  mb_strcut(strip_tags($value['txt']), 0, 168);?>...</p>
                                <div class="data"><?= $value['date'] ?></div>
                                <a class="plus <?php
                                $n++;
                                echo $p.$n;
                                ?>">
                                    <svg class="p" xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 13 13">
                                        <path fill-rule="evenodd" d="M13 4.875v3.25H8.125V13h-3.25V8.125H0v-3.25h4.875V0h3.25v4.875z"/>
                                    </svg>
                                    <svg class="m" xmlns="http://www.w3.org/2000/svg" width="13" height="5" viewBox="0 0 13 5">
                                        <path fill-rule="evenodd" d="M13 .875v3.25H0V.875h8.125z"/>
                                    </svg>
                                </a>
                            </div>
                            <div class="inform-s4 <?php
                            echo $i.$n;
                            ?> slider__inform-block ">
                                <div class="scrollbar scroll-s4">
                                    <div class="list force-overflow">
                                        <div class="scan"><img  src="<?= $value['img-doc'] ?>" alt=""></div>
                                    </div>
                                </div>
                                <div class="full-screen" data-src_doc="<?= $value['img-doc'] ?>">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="0 0 27 27">
                                        <path fill-rule="evenodd" d="M10.8 24.3H4.609l6.191-6.191L8.891 16.2 2.7 22.391V16.2H0V27h10.8v-2.7zM27 0H16.2v2.7h6.191L16.25 8.841l1.909 1.91L24.3 4.608V10.8H27V0z"/>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
                <!--<div class="b-slid-block"></div>-->
                <!-- Add Arrows -->
            </div>
            <div class="swiper-button-next">
                <svg xmlns="http://www.w3.org/2000/svg" width="36" height="20" viewBox="0 0 36 20">
                    <path  fill-rule="evenodd" d="M36 9.576L25.817 0l-2.545 2.212 5.838 5.472H0v3.561h29.11l-5.838 6.083L25.817 20z"/>
                </svg>
            </div>
            <div class="swiper-button-prev">
                <svg xmlns="http://www.w3.org/2000/svg" width="36" height="20" viewBox="0 0 36 20">
                    <path fill-rule="evenodd" d="M10.183 20l2.545-2.782-5.836-5.99H36V7.72H6.892l5.836-5.387L10.183 0 0 9.547z"/>
                </svg>
            </div>
        </div>

        <?=  get_search_form() ; ?>

        <div class="section6">
            <div class="partners-slider">
                <div class="swiper-container swiper-sl3">
                    <div class="swiper-wrapper">
                        <?php foreach (get_field('gfhnyth') as $key => $value): ?>
                            <div class="swiper-slide Group">
                                <img src="<?= $value['img'] ?>" alt="">
                            </div>
                        <?php endforeach ?>

                    </div>
                </div>
            </div>
        </div>

    </main>



<?php
get_footer();
